<!DOCTYPE html>
<html>

    <head>
        <? $title = 'Estatísticas do uHelp';?>  
        <? include 'header.php';?>
    </head>
    
    <body>
        <? include 'navbar.php';?>
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="span3" id="sidebar">
                    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
                        <li>
                            <a href="index.php"><i class="icon-chevron-right"></i> Painel de Controle</a>
                        </li>
                        <li>
                            <a href="tasks.php"><i class="icon-chevron-right"></i> Tarefas</a>
                        </li>
                        <li class="active">
                            <a href="stats.php"><i class="icon-chevron-right"></i> Estatísticas</a>
                        </li>
                        <li>
                            <a href="usrs.php"><span class="badge badge-success pull-right">8</span> Usuários</a>
                        </li>
                        <li>
                            <a href="posts.php"><span class="badge badge-success pull-right">12</span> Postagens</a>
                        </li>
                        <li>
                            <a href="comments.php"><span class="badge badge-success pull-right">50</span> Comentários</a>
                        </li>
                        <li>
                            <a href="hashtags.php"><span class="badge badge-success pull-right">30</span> HashTags</a>
                        </li>
                    </ul>
                </div>

                <div class="span9" id="content">
                      <!-- morris stacked chart -->
                    <div class="row-fluid">
                        <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                <a href=""><div class="pull-left">Relação entre o número de Usuários, Postagens, Comentários e HashTags</div></a>
                            </div>
                            <div class="block-content collapse in">
                                <div class="span12">
                                    <div id="hero-area" style="height: 250px;"></div>
                                </div>
                            </div>
                        </div>
                        <!-- /block -->
                    </div>

                    <!-- jQuery knobs -->
                    <div class="row-fluid section">
                         <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left">As HashTags mais utilizadas</div>
                            </div>
                            <div class="block-content collapse in">
                                <div class="span3">
                                    <input value="50" class="knob second" data-thickness=".3" data-inputColor="#333" data-fgColor="#30a1ec" data-bgColor="#d4ecfd" data-width="140" data-readOnly=true>
                                    <div class="chart-bottom-heading" style="width: 142px;position:absolute;top:70%;">
                                        <span class="label label-info">#BuracoNaRua</span>
                                    </div>
                                </div>
                                <div class="span3">
                                    <input type="text" value="75" class="knob second" data-thickness=".3" data-inputColor="#333" data-fgColor="#8ac368" data-bgColor="#c4e9aa" data-width="140" data-readOnly=true>
                                    <div class="chart-bottom-heading" style="width: 142px;position:absolute;top:70%;">
                                        <span class="label label-info">#SemEnergia</span>
                                    </div>
                                </div>
                                <div class="span3">
                                    <input type="text" value="35" class="knob second" data-thickness=".3" data-inputColor="#333" data-fgColor="#5ba0a3" data-bgColor="#cef3f5" data-width="140" data-readOnly=true>
                                    <div class="chart-bottom-heading" style="width: 142px;position:absolute;top:70%;">
                                        <span class="label label-info">#ÔnibusQuebrado</span>
                                    </div>
                                </div>
                                <div class="span3">
                                    <input type="text" value="85" class="knob second" data-thickness=".3" data-inputColor="#333" data-fgColor="#b85e80" data-bgColor="#f8d2e0" data-width="140" data-readOnly=true>
                                    <div class="chart-bottom-heading" style="width: 142px;position:absolute;top:70%;">
                                        <span class="label label-info">#SemAula</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /block -->
                    </div>

                </div>
            </div>
            <hr>
            <footer>
                <p>&copy; Vincent Gabriel 2013</p>
            </footer>
        </div>
        <!--/.fluid-container-->
        <link rel="stylesheet" href="vendors/morris/morris.css">


        <script src="vendors/jquery-1.9.1.min.js"></script>
        <script src="vendors/jquery.knob.js"></script>
        <script src="vendors/raphael-min.js"></script>
        <script src="vendors/morris/morris.min.js"></script>

        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="vendors/flot/jquery.flot.js"></script>
        <script src="vendors/flot/jquery.flot.categories.js"></script>
        <script src="vendors/flot/jquery.flot.pie.js"></script>
        <script src="vendors/flot/jquery.flot.time.js"></script>
        <script src="vendors/flot/jquery.flot.stack.js"></script>
        <script src="vendors/flot/jquery.flot.resize.js"></script>

        <script src="assets/scripts.js"></script>
        <script>
        $(function() {
            var data = [ ["January", 10], ["February", 8], ["March", 4], ["April", 13], ["May", 17], ["June", 9] ];

            $.plot("#catchart", [ data ], {
                series: {
                    bars: {
                        show: true,
                        barWidth: 0.6,
                        align: "center"
                    }
                },
                xaxis: {
                    mode: "categories",
                    tickLength: 0
                }
            });

            var data = [],
            series = Math.floor(Math.random() * 6) + 3;

            for (var i = 0; i < series; i++) {
                data[i] = {
                    label: "Series" + (i + 1),
                    data: Math.floor(Math.random() * 100) + 1
                }
            }

            $.plot('#piechart1', data, {
                series: {
                    pie: { 
                        show: true,
                        radius: 1,
                        label: {
                            show: true,
                            radius: 3/4,
                            formatter: labelFormatter,
                            background: { 
                                opacity: 0.5,
                                color: '#000'
                            }
                        }
                    }
                },
                legend: {
                    show: false
                }
            });

            $.plot('#piechart2', data, {
                series: {
                    pie: {
                        show: true,
                        radius: 1,
                        tilt: 0.5,
                        label: {
                            show: true,
                            radius: 1,
                            formatter: labelFormatter,
                            background: {
                                opacity: 0.8
                            }
                        },
                        combine: {
                            color: '#999',
                            threshold: 0.1
                        }
                    }
                },
                legend: {
                    show: false
                }
            });

        function euroFormatter(v, axis) {
            return v.toFixed(axis.tickDecimals) + "€";
        }

        function doPlot(position) {
            $.plot("#timechart", [
                { data: oilprices, label: "Oil price ($)" },
                { data: exchangerates, label: "USD/EUR exchange rate", yaxis: 2 }
            ], {
                xaxes: [ { mode: "time" } ],
                yaxes: [ { min: 0 }, {
                    // align if we are to the right
                    alignTicksWithAxis: position == "right" ? 1 : null,
                    position: position,
                    tickFormatter: euroFormatter
                } ],
                legend: { position: "sw" }
            });
        }

        doPlot("right");

        });

        // Morris Area Chart
        Morris.Area({
            element: 'hero-area',
            data: [
                {period: '2010 Q1', users: 2666, posts: null, comments: 2647, tags: null},
                {period: '2010 Q2', users: 2778, posts: 2294, comments: 2441, tags: 2920},
                {period: '2010 Q3', users: 4912, posts: 1969, comments: 2501, tags: 2993},
                {period: '2010 Q4', users: 3767, posts: 3597, comments: 5689, tags: 3500},
                {period: '2011 Q1', users: 6810, posts: 1914, comments: 2293, tags: 3521},
                {period: '2011 Q2', users: 5670, posts: 4293, comments: 1881, tags: 3749},
                {period: '2011 Q3', users: 4820, posts: 3795, comments: 1588, tags: 3875},
                {period: '2011 Q4', users: 15073, posts: 5967, comments: 5175, tags: 4567},
                {period: '2012 Q1', users: 10687, posts: 4460, comments: 2028, tags: 4873},
                {period: '2012 Q2', users: 8432, posts: 5713, comments: 1791, tags: 4980}
            ],
            xkey: 'period',
            ykeys: ['users', 'posts', 'comments', 'tags'],
            labels: ['Usuários', 'Postagens', 'Comentários', 'HashTags'],
            lineWidth: 2,
            hideHover: 'auto',
            lineColors: ["#81d5d9", "#a6e182", "#67bdf8", "#f9696e"]
          });



        // Build jQuery Knobs
        $(".knob").knob();

        function labelFormatter(label, series) {
            return "<div style='font-size:8pt; text-align:center; padding:2px; color:white;'>" + label + "<br/>" + Math.round(series.percent) + "%</div>";
        }
        </script>
    </body>

</html>