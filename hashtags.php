<!DOCTYPE html>
<html>
    
    <head>
        <? $title = 'HashTags em postagens no uHelp';?>  
        <? include 'header.php';?>
    </head>
    
    <body>
        <? include 'navbar.php';?>
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="span3" id="sidebar">
                    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
                        <li>
                            <a href="index.php"><i class="icon-chevron-right"></i> Painel de Controle</a>
                        </li>
                        <li>
                            <a href="tasks.php"><i class="icon-chevron-right"></i> Tarefas</a>
                        </li>
                        <li>
                            <a href="stats.php"><i class="icon-chevron-right"></i> Estatísticas</a>
                        </li>
                        <li>
                            <a href="usrs.php"><span class="badge badge-success pull-right">8</span> Usuários</a>
                        </li>
                        <li>
                            <a href="posts.php"><span class="badge badge-success pull-right">12</span> Postagens</a>
                        </li>
                        <li>
                            <a href="comments.php"><span class="badge badge-success pull-right">50</span> Comentários</a>
                        </li>
                        <li class="active">
                            <a href="hashtags.php"><span class="badge badge-success pull-right">30</span> HashTags</a>
                        </li>
                    </ul>
                </div>

                <div class="span9" id="content">

                     <div class="row-fluid">
                        <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                <a href=""><div class="pull-left">HashTags em postagens no uHelp</div></a>
                            </div>
                            <div class="block-content collapse in">
                                <div class="span12">
  									<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered table-hover" id="example">
										<thead>
											<tr>
												<th>ID</th>
												<th>Nome</th>
												<th>Sobrenome</th>
												<th>Cidade</th>
												<th>Estado</th>
											</tr>
										</thead>
										<tbody>
											<tr class="odd gradeX">
												<td><a href="">12435456345</a></td>
												<td>Marcos</td>
												<td>Assis</td>
												<td class="center"> Feira de Santana</td>
												<td class="center">BA</td>
											</tr>
                                            <tr class="odd gradeX">
                                                <td><a href="">12435456346</a></td>
                                                <td>Afonso</td>
                                                <td>Carvalho</td>
                                                <td class="center"> Salvador</td>
                                                <td class="center">BA</td>
                                            </tr>
                                            <tr class="odd gradeX">
                                                <td><a href="">12435456347</a></td>
                                                <td>Carlos</td>
                                                <td>Machado</td>
                                                <td class="center"> São Paulo</td>
                                                <td class="center">SP</td>
                                            </tr>
                                            <tr class="odd gradeX">
                                                <td><a href="">12435456348</a></td>
                                                <td>Diego</td>
                                                <td>Silva</td>
                                                <td class="center"> Minas Gerais</td>
                                                <td class="center">MG</td>
                                            </tr>
                                            <tr class="odd gradeX">
                                                <td><a href="">12435456349</a></td>
                                                <td>Aécio</td>
                                                <td>Santos</td>
                                                <td class="center"> Barreiras</td>
                                                <td class="center">BA</td>
                                            </tr>
                                            <tr class="odd gradeX">
                                                <td><a href="">12435456350</a></td>
                                                <td>Hérica</td>
                                                <td>Lacerda</td>
                                                <td class="center"> Feira de Santana</td>
                                                <td class="center">BA</td>
                                            </tr>
                                            <tr class="odd gradeX">
                                                <td><a href="">12435456351</a></td>
                                                <td>Camilla</td>
                                                <td>Leite</td>
                                                <td class="center"> Santana</td>
                                                <td class="center">BA</td>
                                            </tr>
                                            <tr class="odd gradeX">
                                                <td><a href="">12435456352</a></td>
                                                <td>Matheus</td>
                                                <td>Souza</td>
                                                <td class="center"> Manaus</td>
                                                <td class="center">AM</td>
                                            </tr>
                                            <tr class="odd gradeX">
                                                <td><a href="">12435456353</a></td>
                                                <td>Antônio</td>
                                                <td>Almeida</td>
                                                <td class="center"> Recife</td>
                                                <td class="center">PE</td>
                                            </tr>
                                            <tr class="odd gradeX">
                                                <td><a href="">12435456354</a></td>
                                                <td>Daniel</td>
                                                <td>Macedo</td>
                                                <td class="center"> Rio de Janeiro</td>
                                                <td class="center">RJ</td>
                                            </tr>

                                            <tr class="odd gradeX">
                                                <td><a href="">12435456355</a></td>
                                                <td>Marcos</td>
                                                <td>Assis</td>
                                                <td class="center"> Feira de Santana</td>
                                                <td class="center">BA</td>
                                            </tr>
                                            <tr class="odd gradeX">
                                                <td><a href="">12435456356</a></td>
                                                <td>Afonso</td>
                                                <td>Carvalho</td>
                                                <td class="center"> Salvador</td>
                                                <td class="center">BA</td>
                                            </tr>
                                            <tr class="odd gradeX">
                                                <td><a href="">12435456357</a></td>
                                                <td>Carlos</td>
                                                <td>Machado</td>
                                                <td class="center"> São Paulo</td>
                                                <td class="center">SP</td>
                                            </tr>
                                            <tr class="odd gradeX">
                                                <td><a href="">12435456358</a></td>
                                                <td>Diego</td>
                                                <td>Silva</td>
                                                <td class="center"> Minas Gerais</td>
                                                <td class="center">MG</td>
                                            </tr>
                                            <tr class="odd gradeX">
                                                <td><a href="">12435456359</a></td>
                                                <td>Aécio</td>
                                                <td>Santos</td>
                                                <td class="center"> Barreiras</td>
                                                <td class="center">BA</td>
                                            </tr>
                                            <tr class="odd gradeX">
                                                <td><a href="">12435456360</a></td>
                                                <td>Hérica</td>
                                                <td>Lacerda</td>
                                                <td class="center"> Feira de Santana</td>
                                                <td class="center">BA</td>
                                            </tr>
                                            <tr class="odd gradeX">
                                                <td><a href="">12435456361</a></td>
                                                <td>Camilla</td>
                                                <td>Leite</td>
                                                <td class="center"> Santana</td>
                                                <td class="center">BA</td>
                                            </tr>
                                            <tr class="odd gradeX">
                                                <td><a href="">12435456362</a></td>
                                                <td>Matheus</td>
                                                <td>Souza</td>
                                                <td class="center"> Manaus</td>
                                                <td class="center">AM</td>
                                            </tr>
                                            <tr class="odd gradeX">
                                                <td><a href="">12435456363</a></td>
                                                <td>Antônio</td>
                                                <td>Almeida</td>
                                                <td class="center"> Recife</td>
                                                <td class="center">PE</td>
                                            </tr>
                                            <tr class="odd gradeX">
                                                <td><a href="">12435456364</a></td>
                                                <td>Daniel</td>
                                                <td>Macedo</td>
                                                <td class="center"> Rio de Janeiro</td>
                                                <td class="center">RJ</td>
                                            </tr>

                                            <tr class="odd gradeX">
                                                <td><a href="">12435456365</a></td>
                                                <td>Marcos</td>
                                                <td>Assis</td>
                                                <td class="center"> Feira de Santana</td>
                                                <td class="center">BA</td>
                                            </tr>
                                            <tr class="odd gradeX">
                                                <td><a href="">12435456366</a></td>
                                                <td>Afonso</td>
                                                <td>Carvalho</td>
                                                <td class="center"> Salvador</td>
                                                <td class="center">BA</td>
                                            </tr>
                                            <tr class="odd gradeX">
                                                <td><a href="">12435456367</a></td>
                                                <td>Carlos</td>
                                                <td>Machado</td>
                                                <td class="center"> São Paulo</td>
                                                <td class="center">SP</td>
                                            </tr>
                                            <tr class="odd gradeX">
                                                <td><a href="">12435456368</a></td>
                                                <td>Diego</td>
                                                <td>Silva</td>
                                                <td class="center"> Minas Gerais</td>
                                                <td class="center">MG</td>
                                            </tr>
                                            <tr class="odd gradeX">
                                                <td><a href="">12435456369</a></td>
                                                <td>Aécio</td>
                                                <td>Santos</td>
                                                <td class="center"> Barreiras</td>
                                                <td class="center">BA</td>
                                            </tr>
                                            <tr class="odd gradeX">
                                                <td><a href="">12435456370</a></td>
                                                <td>Hérica</td>
                                                <td>Lacerda</td>
                                                <td class="center"> Feira de Santana</td>
                                                <td class="center">BA</td>
                                            </tr>
                                            <tr class="odd gradeX">
                                                <td><a href="">12435456371</a></td>
                                                <td>Camilla</td>
                                                <td>Leite</td>
                                                <td class="center"> Santana</td>
                                                <td class="center">BA</td>
                                            </tr>
                                            <tr class="odd gradeX">
                                                <td><a href="">12435456372</a></td>
                                                <td>Matheus</td>
                                                <td>Souza</td>
                                                <td class="center"> Manaus</td>
                                                <td class="center">AM</td>
                                            </tr>
                                            <tr class="odd gradeX">
                                                <td><a href="">12435456373</a></td>
                                                <td>Antônio</td>
                                                <td>Almeida</td>
                                                <td class="center"> Recife</td>
                                                <td class="center">PE</td>
                                            </tr>
                                            <tr class="odd gradeX">
                                                <td><a href="">12435456374</a></td>
                                                <td>Daniel</td>
                                                <td>Macedo</td>
                                                <td class="center"> Rio de Janeiro</td>
                                                <td class="center">RJ</td>
                                            </tr>
										</tbody>
									</table>
                                </div>
                            </div>
                        </div>
                        <!-- /block -->
                    </div>


                </div>
            </div>
            <hr>
            <footer>
                <p>&copy; Vincent Gabriel 2013</p>
            </footer>
        </div>
        <!--/.fluid-container-->

        <script src="vendors/jquery-1.9.1.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="vendors/datatables/js/jquery.dataTables.min.js"></script>


        <script src="assets/scripts.js"></script>
        <script src="assets/DT_bootstrap.js"></script>
        <script>
        $(function() {
            
        });
        </script>
    </body>

</html>