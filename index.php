<!DOCTYPE html>
<html class="no-js">

    <head>
        <? $title = 'Painel de Controle';?>  
        <? include 'header.php';?>
    </head>
    
    <body>
        <? include 'navbar.php';?>
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="span3" id="sidebar">
                    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
                        <li class="active">
                            <a href="index.php"><i class="icon-chevron-right"></i> Painel de Controle</a>
                        </li>
                        <li>
                            <a href="tasks.php"><i class="icon-chevron-right"></i> Tarefas</a>
                        </li>
                        <li>
                            <a href="stats.php"><i class="icon-chevron-right"></i> Estatísticas</a>
                        </li>
                        <li>
                            <a href="usrs.php"><span class="badge badge-success pull-right">8</span> Usuários</a>
                        </li>
                        <li>
                            <a href="posts.php"><span class="badge badge-success pull-right">12</span> Postagens</a>
                        </li>
                        <li>
                            <a href="comments.php"><span class="badge badge-success pull-right">50</span> Comentários</a>
                        </li>
                        <li>
                            <a href="hashtags.php"><span class="badge badge-success pull-right">30</span> HashTags</a>
                        </li>
                    </ul>
                </div>
                <div class="span9" id="content">
                    <div class="row-fluid">
                            <div class="navbar">
                                <div class="navbar-inner">
                                    <ul class="breadcrumb">
                                        <i class="icon-chevron-left hide-sidebar"><a href='#' title="Hide Sidebar" rel='tooltip'>&nbsp;</a></i>
                                        <i class="icon-chevron-right show-sidebar" style="display:none;"><a href='#' title="Show Sidebar" rel='tooltip'>&nbsp;</a></i>
                                        <li>
                                            <a href="">Visão geral do sistema</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    <div class="row-fluid">
                        <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left">Números do uHelp</div>
                                <a href="stats.php"><div class="pull-right"><span class="badge badge-warning">Veja mais</span></a>

                                </div>
                            </div>
                            <div class="block-content collapse in">
                                <div class="span3">
                                    <!-- X percent of the graph but X numbers of registers -->
                                    <div class="chart" data-percent="8">8</div>
                                    <div class="chart-bottom-heading"><span class="label label-info">Usuários</span>

                                    </div>
                                </div>
                                <div class="span3">
                                    <div class="chart" data-percent="12">12</div>
                                    <div class="chart-bottom-heading"><span class="label label-info">Postagens</span>

                                    </div>
                                </div>
                                <div class="span3">
                                    <div class="chart" data-percent="50">50</div>
                                    <div class="chart-bottom-heading"><span class="label label-info">Comentários</span>

                                    </div>
                                </div>
                                <div class="span3">
                                    <div class="chart" data-percent="30">30</div>
                                    <div class="chart-bottom-heading"><span class="label label-info">HashTags</span>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /block -->
                    </div>
                    <div class="row-fluid">
                        <div class="span6">
                            <!-- block -->
                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                    <div class="muted pull-left">Usuários</div>
                                    <div class="pull-right"><span class="badge badge-info">8</span>

                                    </div>
                                </div>
                                <div class="block-content collapse in">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Nome</th>
                                                <th>Sobrenome</th>
                                                <th>Usuário</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>Maria</td>
                                                <td>Aparecida</td>
                                                <td>mariaapa@gmail.com</td>
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td>Jhonata</td>
                                                <td>Eduardo</td>
                                                <td>jhonata_fsa@hotmail.com</td>
                                            </tr>
                                            <tr>
                                                <td>3</td>
                                                <td>Vincente</td>
                                                <td>Araujo</td>
                                                <td>vincentao@yahoo.com</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /block -->
                        </div>
                        <div class="span6">
                            <!-- block -->
                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                    <div class="muted pull-left">HashTags</div>
                                    <div class="pull-right"><span class="badge badge-info">30</span>

                                    </div>
                                </div>
                                <div class="block-content collapse in">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>HashTag</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>#NemTeConto</td>
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td>#MoriçocaHater</td>
                                            </tr>
                                            <tr>
                                                <td>3</td>
                                                <td>#EscuridãoNaMangabeira</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /block -->
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="span6">
                            <!-- block -->
                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                    <div class="muted pull-left">Postagens</div>
                                    <div class="pull-right"><span class="badge badge-info">12</span>

                                    </div>
                                </div>
                                <div class="block-content collapse in">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Título</th>
                                                <th>Resumo</th>
                                                <th>Autor</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>Buraco no Tomba</td>
                                                <!-- 50 é o limite de caracteres -->
                                                <td>Depois de chuva intensa, cratera enorme toma conta...</td>
                                                <td>Joao Pereira</td>
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td>Queda energia</td>
                                                <td>Motorista bêbado bate caminhão em poste e deixa b...</td>
                                                <td>Maria Silva</td>
                                            </tr>
                                            <tr>
                                                <td>3</td>
                                                <td>Moricoca</td>
                                                <td>Obras de esgotamento, inacabadas, aumentam o índic...</td>
                                                <td>Jose Raimundo</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /block -->
                        </div>
                        <div class="span6">
                            <!-- block -->
                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                    <div class="muted pull-left">Comentários</div>
                                    <div class="pull-right"><span class="badge badge-info">50</span>

                                    </div>
                                </div>
                                <div class="block-content collapse in">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Autor</th>
                                                <th>Comentário</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>Maria Aparecida</td>
                                                <td>Não acredito que o prefeito da cidade deixou ess...</td>
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td>Murilo Oliveira</td>
                                                <td>As muriçocas estão me carregando.</td>
                                            </tr>
                                            <tr>
                                                <td>3</td>
                                                <td>Vincente Araujo</td>
                                                <td>Nossa! Não aguento mais ficar sem energia elétrica.</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /block -->
                        </div>
                    </div>
                    <div class="row-fluid"></div>
                </div>
            </div>
            <hr>
            <footer>
                <p>&copy; Vincent Gabriel 2013</p>
            </footer>
        </div>
        <!--/.fluid-container-->
        <script src="vendors/jquery-1.9.1.min.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="vendors/easypiechart/jquery.easy-pie-chart.js"></script>
        <script src="assets/scripts.js"></script>
        <script>
        $(function() {
            // Easy pie charts
            $('.chart').easyPieChart({animate: 1000});
        });
        </script>
    </body>

</html>